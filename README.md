# Computer Graphics Lighting and Shading Models

Culling hidden lines and surfaces, back-face culling. Phong lighting model and constant, Gouraud and Phong shading models. Implemented in Python using NumPy library and pyglet, Python OpenGL interface.

My lab assignment in Interactive Computer Graphics, FER, Zagreb.

Docs in "DokumentacijaIRGLabosi" under "7. VJEZBA."

Created: 2020
