import numpy as np
from math import sqrt

from os import path
import sys
from pyglet.gl import *
from pyglet.window import key

vertexDefsINIT=[]
vertexDefs=[]
polyDefs=[]

global Ociste
global OcisteINIT
global IzvorINIT
global Glediste
global planeCoeffs
polyCenterPointsINIT = []
global polyNormals
global polyCenterPoints

width = 700
height = 700

# otvara prozor
config = pyglet.gl.Config(double_buffer=True)
window = pyglet.window.Window(width=width, height=height, caption="3d transforms", resizable=True, config=config, visible=False)
window.set_location(100, 100)
window.set_minimum_size(300, 300)


def loadOG(ogDescr):
    global Ociste
    global Glediste
    Ociste = list(map(float, ogDescr.readline().split()[1:]))
    Glediste = list(map(float, ogDescr.readline().split()[1:]))

    Ociste.append(1)
    Glediste.append(1)

    global OcisteINIT
    OcisteINIT = Ociste[:]

def loadVertices(objVerticesDescr):
    global vertexDefsINIT
    global polyDefs

    for line in objVerticesDescr:

        if line.startswith("v"):
            print(line, end='')
            vertexDefsINIT.append(line.split()[1:])
        elif line.startswith("f"):
            print(line, end='')
            polyDefs.append(line.split()[1:])

    vertexDefsINIT = [list(map(float, x)) for x in vertexDefsINIT]
    polyDefs = [list(map(int, x)) for x in polyDefs]

def loadShadingParams(shadingParamsDescr):
    global Ia, Id, ka, kd, Izvor
    Ia = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]
    Id = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]
    ka = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]
    kd = list(map(float, shadingParamsDescr.readline().split()[1:]))[0]
    Izvor = list(map(float, shadingParamsDescr.readline().split()[1:]))
    Izvor.append(1)

    # print(Ia)
    # print(Id)
    # print(ka)
    # print(kd)
    # print(Izvor)

    global IzvorINIT
    IzvorINIT = Izvor[:]


def getViewMatrix(O, G):
    VG = np.array([G[0], G[1], G[2], 1])
    T1 = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[-O[0], -O[1], -O[2], 1]])

    G1 = VG.dot(T1)
    xG1, yG1, zG1, _ = G1

    if yG1 != 0:
        sinAlpha = yG1 / (sqrt(xG1**2 + yG1**2))
        cosAlpha = xG1 / (sqrt(xG1**2 + yG1**2))
        T2 = np.array([[cosAlpha,-sinAlpha,0,0],[sinAlpha,cosAlpha,0,0],[0,0,1,0],[0,0,0,1]])
    else:
        T2 = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

    G2 = G1.dot(T2)
    xG2, yG2, zG2, _ = G2

    if xG2 != 0:
        sinBeta = xG2 / (sqrt(xG2**2 + zG2**2))
        cosBeta = zG2 / (sqrt(xG2**2 + zG2**2))
        T3 = np.array([[cosBeta,0,sinBeta,0],[0,1,0,0],[-sinBeta,0,cosBeta,0],[0,0,0,1]])
    else:
        T3 = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])


    T4 = np.array([[0,-1,0,0],[1,0,0,0],[0,0,1,0],[0,0,0,1]])

    T5 = np.array([[-1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])

    T = T1.dot(T2).dot(T3).dot(T4).dot(T5)

    return T

def getPerspectiveProjectionMatrix(O, G):
    H = sqrt((O[0]-G[0])**2 + (O[1]-G[1])**2 + (O[2]-G[2])**2)

    if H==0:
        print("Ociste i glediste se poklapaju!")
        return np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

    P = np.array([[1,0,0,0],[0,1,0,0],[0,0,0,1/H],[0,0,0,0]])

    return P


def translateObject(vertices, dx, dy, dz):
    return [[v[0]+dx, v[1]+dy, v[2]+dz] for v in vertices]

def scaleObject(vertices, scaleFactor):
    return [[v[0]*scaleFactor, v[1]*scaleFactor, v[2]*scaleFactor] for v in vertices]

def calcVertexNormals():
    vertNormals = [[[0, 0, 0], 0] for x in range(len(vertexDefsINIT))]  # ((a, b, c), N) - a, b, c (normal in point coeffs), N - nr. of incident polygons
    polyDefsLen = len(polyDefs)
    for i in range(polyDefsLen):
        p = polyDefs[i]
        n = polyNormals[i]
        a, b, c = n[0], n[1], n[2]

        point0Normal = vertNormals[p[0] - 1]
        point1Normal = vertNormals[p[1] - 1]
        point2Normal = vertNormals[p[2] - 1]

        point0Normal[0][0] += a
        point0Normal[0][1] += b
        point0Normal[0][2] += c

        point1Normal[0][0] += a
        point1Normal[0][1] += b
        point1Normal[0][2] += c

        point2Normal[0][0] += a
        point2Normal[0][1] += b
        point2Normal[0][2] += c

        # one more incident polygon for each polygon point
        point0Normal[1] += 1
        point1Normal[1] += 1
        point2Normal[1] += 1

    global vertNormalsABC
    vertNormalsABC = []
    for vn in vertNormals:
        normal = vn[0]
        numIncidentPolygs = vn[1]
        vertNormalsABC.append([normal[0]/numIncidentPolygs,normal[1]/numIncidentPolygs,normal[2]/numIncidentPolygs])


@window.event
def on_draw():
    window.clear()

    vs = scaleObject(vertexDefs, 100)
    vs = translateObject(vs, width/2, height/2, 0)

    glColor3f(0.0, 0.0, 0.0)
    glBegin(GL_TRIANGLES)

    polyDefsLen = len(polyDefs)
    for i in range(polyDefsLen):
        if polygonBack(i):
            continue

        p = polyDefs[i]
        point0 = vs[p[0] - 1]
        point1 = vs[p[1] - 1]
        point2 = vs[p[2] - 1]

        point0Normal = vertNormalsABC[p[0] - 1]
        point1Normal = vertNormalsABC[p[1] - 1]
        point2Normal = vertNormalsABC[p[2] - 1]

        polyCenterIzvorVect = getPolyCenterIzvorVect(i)

        intensity0 = getLightIntensity(polyCenterIzvorVect, point0Normal)
        intensity1 = getLightIntensity(polyCenterIzvorVect, point1Normal)
        intensity2 = getLightIntensity(polyCenterIzvorVect, point2Normal)

        glColor3ub(int(intensity0), 0, 0)
        glVertex2f(point0[0], point0[1])

        glColor3ub(int(intensity1), 0, 0)
        glVertex2f(point1[0], point1[1])

        glColor3ub(int(intensity2), 0, 0)
        glVertex2f(point2[0], point2[1])

    glEnd()

    glFlush()


def getLightIntensity(polyCenterIzvorVect, polyNormal):
    L = getNormalizedVect(polyCenterIzvorVect)
    N = getNormalizedVect(polyNormal)
    LN = getScalarProduct(L, N)
    I = Ia*ka + Id*kd*max(0, LN)

    # print("L=",L)
    # print("N=", N)
    # print("LN=",LN)
    # print("I=",I)

    return I


def getScalarProduct(vect1, vect2):
    return vect1[0]*vect2[0] + vect1[1]*vect2[1] + vect1[2]*vect2[2]

def getVectNorm(vect):
    return sqrt(vect[0]**2 + vect[1]**2 + vect[2]**2)

def getNormalizedVect(vect):
    vectNorm = getVectNorm(vect)
    return [vect[0]/vectNorm,vect[1]/vectNorm,vect[2]/vectNorm]

def getPolyCenterIzvorVect(polyNum):
    polyCenterPoint = polyCenterPoints[polyNum]

    polyCenterIzvorVect = [Izvor[0] - polyCenterPoint[0], Izvor[1] - polyCenterPoint[1],
                            Izvor[2] - polyCenterPoint[2], Izvor[3] - polyCenterPoint[3]]

    return polyCenterIzvorVect

def getPolyCenterOcisteVect(polyNum):
    polyCenterPoint = polyCenterPoints[polyNum]

    polyCenterOcisteVect = [Ociste[0] - polyCenterPoint[0], Ociste[1] - polyCenterPoint[1],
                            Ociste[2] - polyCenterPoint[2], Ociste[3] - polyCenterPoint[3]]

    return polyCenterOcisteVect


def polygonBack(polyNum):
    polyCenterOcisteVect = getPolyCenterOcisteVect(polyNum)
    normalVect = polyNormals[polyNum]

    scalarProd = polyCenterOcisteVect[0]*normalVect[0]+polyCenterOcisteVect[1]*normalVect[1]+polyCenterOcisteVect[2]*normalVect[2]

    return scalarProd < 0


@window.event
def on_resize(w, h):
    global width, height
    width = w
    height = h
    glViewport(0, 0, width, height)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(0, width, 0, height)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    glClearColor(1.0, 1.0, 1.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glPointSize(3.0)
    glColor3f(0.0, 0.0, 0.0)


@window.event
def on_key_press(symbol, modifiers):
    global Ociste
    global Izvor
    if symbol == key.Q: # xO +
        Ociste[0] += 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.A: # xO -
        Ociste[0] -= 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.W: # yO +
        Ociste[1] += 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.S: # yO -
        Ociste[1] -= 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.E: # zO +
        Ociste[2] += 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.D: # zO -
        Ociste[2] -= 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOGChange()
    elif symbol == key.T: # xIzvor +
        Izvor[0] += 1
        print("Izvor = ({}, {}, {})".format(*Izvor))
        onOGChange()
    elif symbol == key.G: # xIzvor -
        Izvor[0] -= 1
        print("Izvor = ({}, {}, {})".format(*Izvor))
        onOGChange()
    elif symbol == key.Z: # yIzvor +
        Izvor[1] += 1
        print("Izvor = ({}, {}, {})".format(*Izvor))
        onOGChange()
    elif symbol == key.H: # yIzvor -
        Izvor[1] -= 1
        print("Izvor = ({}, {}, {})".format(*Izvor))
        onOGChange()
    elif symbol == key.U: # zIzvor +
        Izvor[2] += 1
        print("Izvor = ({}, {}, {})".format(*Izvor))
        onOGChange()
    elif symbol == key.J: # zIzvor -
        Izvor[2] -= 1
        print("Izvor = ({}, {}, {})".format(*Izvor))
        onOGChange()
    elif symbol == key._0:  # reset na pocetni O i Izvor
        print("Reset na pocetno ociste i izvor...")
        Ociste = OcisteINIT[:]
        Izvor = IzvorINIT[:]
        onOGChange()
    elif symbol == key.ESCAPE:
        sys.exit()

    glFlush()

def getCenterRange(vs):
    xCoords = [v[0] for v in vs]
    yCoords = [v[1] for v in vs]
    zCoords = [v[2] for v in vs]

    xmin, xmax, ymin, ymax, zmin, zmax = min(xCoords), max(xCoords), min(yCoords), max(yCoords), min(zCoords), max(
        zCoords)

    xCenter = (xmin + xmax) / 2
    yCenter = (ymin + ymax) / 2
    zCenter = (zmin + zmax) / 2


    maxRange = max(xmax-xmin, ymax-ymin, zmax-zmin)

    return xCenter, yCenter, zCenter, maxRange

def onOGChange():
    if checkTestPointWithinBody(Ociste):
        print("Ociste je unutar tijela!")
    else:
        print("Ociste ",Ociste," je ok.")

    T = getViewMatrix(Ociste, Glediste)
    P = getPerspectiveProjectionMatrix(Ociste, Glediste)

    verticesO = vertexDefsINIT.dot(T)
    verticesP = verticesO.dot(P)


    global vertexDefs
    vertexDefs = []

    vertexDefsNew = []
    for x in verticesP:
        if (x[3] == 0):
            vertexDefsNew.append([x[0], x[1], x[2], 0])
            continue
        vertexDefsNew.append([x[0]/x[3],x[1]/x[3],x[2]/x[3],1])

    vertexDefs = vertexDefsNew

def getTriangleCenter(pts):
    xC = sum([v[0] for v in pts]) / 3
    yC = sum([v[1] for v in pts]) / 3
    zC = sum([v[2] for v in pts]) / 3

    return xC, yC, zC



def calcPlaneEquations(vertices):
    global planeCoeffs
    global polyNormals
    global polyCenterPoints
    planeCoeffs = []
    polyNormals = []
    polyCenterPoints = []
    for p in polyDefs:
        point1 = vertices[p[0] - 1]
        point2 = vertices[p[1] - 1]
        point3 = vertices[p[2] - 1]
        a = (point2[1]-point1[1])*(point3[2]-point1[2]) - (point2[2]-point1[2])*(point3[1]-point1[1])
        b = -(point2[0]-point1[0])*(point3[2]-point1[2]) + (point2[2]-point1[2])*(point3[0]-point1[0])
        c = (point2[0]-point1[0])*(point3[1]-point1[1]) - (point2[1]-point1[1])*(point3[0]-point1[0])
        d = -a * point1[0] - b * point1[1] - c * point1[2]
        planeCoeffs.append((a, b, c, d))


        polyNormals.append([a, b, c])
        xC, yC, zC = getTriangleCenter([point1, point2, point3])
        polyCenterPoints.append([xC, yC, zC])


    polyCenterPointsHom = [[v[0], v[1], v[2], 1] for v in polyCenterPoints]
    polyCenterPointsArr = np.array(polyCenterPointsHom)
    polyCenterPoints = polyCenterPointsArr


    polyNormalsHom = [[n[0], n[1], n[2], 0] for n in polyNormals]
    polyNormalsArr = np.array(polyNormalsHom)
    polyNormals = polyNormalsArr

    return planeCoeffs

def checkTestPointWithinBody(testPnt):
    for plane in planeCoeffsINIT:
        r = plane[0] * testPnt[0] + plane[1] * testPnt[1] + plane[2] * testPnt[2] + plane[3]
        if r > 0:
            return False
    return True


def enterObjFilename():
    objFilename = input("Ime .obj datoteke objekta za ucitati (prazno za default: kocka.obj): ")
    if len(objFilename) == 0:
        return "kocka.obj"
    objFilepath = "./" + objFilename
    while not(path.exists(objFilepath) and objFilepath.endswith(".obj")):
        print("Datoteka " + objFilename + " ne postoji.")
        objFilename = input("Ime .obj datoteke objekta za ucitati (prazno za default: kocka.obj): ")
        if len(objFilename) == 0:
            return "kocka.obj"
        objFilepath = "./" + objFilename

    return objFilepath

def enterOGFilename():
    OGFilename = input("Ime .txt datoteke ocista i gledista za ucitati (prazno za default: initOG.txt): ")
    if len(OGFilename) == 0:
        return "initOG.txt"
    OGFilepath = "./" + OGFilename
    while not(path.exists(OGFilepath) and OGFilepath.endswith(".txt")):
        print("Datoteka " + OGFilename + " ne postoji.")
        OGFilename = input("Ime .txt datoteke ocista i gledista za ucitati (prazno za default: initOG.txt): ")
        if len(OGFilename) == 0:
            return "initOG.txt"
        OGFilepath = "./" + OGFilename

    return OGFilepath


def enterShadingParamsFilename():
    shadingParamsFilename = input("Ime .txt datoteke parametara sjencanja (Ia, ka, Id, kd, Izvor (x, y, z)) za ucitati (prazno za default: shadingParams.txt): ")
    if len(shadingParamsFilename) == 0:
        return "shadingParams.txt"
    shadingParamsFilepath = "./" + shadingParamsFilename
    while not(path.exists(shadingParamsFilepath) and shadingParamsFilepath.endswith(".txt")):
        print("Datoteka " + shadingParamsFilename + " ne postoji.")
        shadingParamsFilename = input("Ime .txt datoteke parametara sjencanja (Ia, ka, Id, kd) za ucitati (prazno za default: shadingParams.txt): ")
        if len(shadingParamsFilename) == 0:
            return "shadingParams.txt"
        shadingParamsFilepath = "./" + shadingParamsFilename

    return shadingParamsFilepath



if __name__ == "__main__":
    OGFilepath = enterOGFilename()
    print("Ucitavanje datoteke ",OGFilepath,"...")
    with open(OGFilepath, "r") as ogDescr:
        loadOG(ogDescr)


    objFilepath = enterObjFilename()
    print("Ucitavanje datoteke ", objFilepath, "...")
    with open(objFilepath, "r") as objVerticesDescr:
        loadVertices(objVerticesDescr)

    shadingParamsFilepath = enterShadingParamsFilename()
    print("Ucitavanje datoteke ", shadingParamsFilepath, "...")
    with open(shadingParamsFilepath, "r") as shadingParamsDescr:
        loadShadingParams(shadingParamsDescr)


    xc, yc, zc, maxRange = getCenterRange(vertexDefsINIT)

    print("Translacija sredista objekta u ishodiste...")
    vertexDefsINIT = translateObject(vertexDefsINIT, -xc, -yc, -zc)

    print("Skaliranje na [-1, 1]...")
    vertexDefsINIT = scaleObject(vertexDefsINIT, 2 / maxRange)

    print()
    print("Translatiran i skaliran objekt:")
    for v in vertexDefsINIT:
        print("v {} {} {}".format(*v))
    for p in polyDefs:
        print("f {} {} {}".format(*p))


    verticesHom = [[v[0],v[1],v[2],1] for v in vertexDefsINIT]
    verticesArr = np.array(verticesHom)
    vertexDefsINIT = verticesArr

    planeCoeffsINIT = calcPlaneEquations(vertexDefsINIT)[:]
    global vertNormalsABC
    calcVertexNormals()
    onOGChange()


    window.set_visible(True)

    pyglet.app.run()