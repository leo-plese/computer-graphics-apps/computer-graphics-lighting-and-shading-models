import numpy as np
from math import sqrt

from os import path
import sys
from pyglet.gl import *
from pyglet.window import key
from pyglet.window import mouse

vertexDefsINIT=[]
polyDefs=[]

global Ociste
global OcisteINIT
global Glediste
global planeCoeffs

doBackCull = False

width = 700
height = 700

# otvara prozor
config = pyglet.gl.Config(double_buffer=True)
window = pyglet.window.Window(width=width, height=height, caption="3d transforms", resizable=True, config=config, visible=False)
window.set_location(100, 100)
window.set_minimum_size(300, 300)

def loadOG(ogDescr):
    global Ociste
    global Glediste
    Ociste = list(map(float, ogDescr.readline().split()[1:]))
    Glediste = list(map(float, ogDescr.readline().split()[1:]))

    Ociste.append(1)
    Glediste.append(1)

    global OcisteINIT
    OcisteINIT = Ociste[:]

def loadVertices(objVerticesDescr):
    global vertexDefsINIT
    global polyDefs

    for line in objVerticesDescr:

        if line.startswith("v"):
            print(line, end='')
            vertexDefsINIT.append(line.split()[1:])
        elif line.startswith("f"):
            print(line, end='')
            polyDefs.append(line.split()[1:])

    vertexDefsINIT = [list(map(float, x)) for x in vertexDefsINIT]
    polyDefs = [list(map(int, x)) for x in polyDefs]


def translateObject(vertices, dx, dy, dz):
    return [[v[0]+dx, v[1]+dy, v[2]+dz] for v in vertices]

def scaleObject(vertices, scaleFactor):
    return [[v[0]*scaleFactor, v[1]*scaleFactor, v[2]*scaleFactor] for v in vertices]



@window.event
def on_draw():
    window.clear()

    updatePerspective()


    glColor3f(0.0, 0.0, 0.0)
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
    glBegin(GL_TRIANGLES)

    polyDefsLen = len(polyDefs)
    for i in range(polyDefsLen):
        p = polyDefs[i]
        point0 = vertexDefsINIT[p[0] - 1]
        point1 = vertexDefsINIT[p[1] - 1]
        point2 = vertexDefsINIT[p[2] - 1]

        glVertex3f(point0[0], point0[1], point0[2])
        glVertex3f(point1[0], point1[1], point1[2])
        glVertex3f(point2[0], point2[1], point2[2])

    glEnd()


    glFlush()


@window.event
def on_resize(w, h):
    global width, height
    width = w
    height = h
    glViewport(0, 0, width, height)

    glClearColor(1.0, 1.0, 1.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glPointSize(1.0)
    glColor3f(0.0, 0.0, 0.0)




def updatePerspective():
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, width/height, 0.5, 8.0) # kut pogleda, x/y, prednja i straznja ravnina odsjecanja
    gluLookAt(Ociste[0],Ociste[1],Ociste[2],Glediste[0],Glediste[1],Glediste[2],0.0,1.0,0.0) # ociste x,y,z; glediste x,y,z; up vektor x,y,z
    glMatrixMode(GL_MODELVIEW)

@window.event
def on_mouse_press(x, y, button, modifiers):
    global Ociste

    # desna tipka - brise canvas
    if button & mouse.RIGHT:
        Ociste[0] = 0
        Ociste[1] = 0
        Ociste[2] = 0
        updatePerspective()


@window.event
def on_key_press(symbol, modifiers):
    global Ociste
    if symbol == key.Q: # xO +
        Ociste[0] += 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOChange()
    elif symbol == key.A: # xO -
        Ociste[0] -= 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOChange()
    elif symbol == key.W: # yO +
        Ociste[1] += 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOChange()
    elif symbol == key.S: # yO -
        Ociste[1] -= 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOChange()
    elif symbol == key.E: # zO +
        Ociste[2] += 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOChange()
    elif symbol == key.D: # zO -
        Ociste[2] -= 1
        print("Ociste = ({}, {}, {})".format(*Ociste))
        onOChange()
    elif symbol == key._0:  # reset na pocetni O i G
        print("Reset na pocetno ociste...")
        Ociste = OcisteINIT[:]
        onOChange()
    elif symbol == key.ESCAPE:
        sys.exit()

    glFlush()

def getCenterRange(vs):
    xCoords = [v[0] for v in vs]
    yCoords = [v[1] for v in vs]
    zCoords = [v[2] for v in vs]

    xmin, xmax, ymin, ymax, zmin, zmax = min(xCoords), max(xCoords), min(yCoords), max(yCoords), min(zCoords), max(
        zCoords)

    xCenter = (xmin + xmax) / 2
    yCenter = (ymin + ymax) / 2
    zCenter = (zmin + zmax) / 2


    maxRange = max(xmax-xmin, ymax-ymin, zmax-zmin)

    return xCenter, yCenter, zCenter, maxRange

def onOChange():
    if checkTestPointWithinBody(Ociste):
        print("Ociste je unutar tijela!")
    else:
        print("Ociste ",Ociste," je ok.")

    updatePerspective()


def calcPlaneEquations(vertices):
    global planeCoeffs
    global polyNormals
    global polyCenterPoints
    planeCoeffs = []
    polyNormals = []
    polyCenterPoints = []
    for p in polyDefs:
        point1 = vertices[p[0] - 1]
        point2 = vertices[p[1] - 1]
        point3 = vertices[p[2] - 1]
        a = (point2[1]-point1[1])*(point3[2]-point1[2]) - (point2[2]-point1[2])*(point3[1]-point1[1])
        b = -(point2[0]-point1[0])*(point3[2]-point1[2]) + (point2[2]-point1[2])*(point3[0]-point1[0])
        c = (point2[0]-point1[0])*(point3[1]-point1[1]) - (point2[1]-point1[1])*(point3[0]-point1[0])
        d = -a * point1[0] - b * point1[1] - c * point1[2]
        planeCoeffs.append((a, b, c, d))

    return planeCoeffs

def checkTestPointWithinBody(testPnt):
    for plane in planeCoeffsINIT:
        r = plane[0] * testPnt[0] + plane[1] * testPnt[1] + plane[2] * testPnt[2] + plane[3]
        if r > 0:
            return False
    return True


def enterObjFilename():
    objFilename = input("Ime .obj datoteke objekta za ucitati (prazno za default: kocka.obj): ")
    if len(objFilename) == 0:
        return "kocka.obj"
    objFilepath = "./" + objFilename
    while not(path.exists(objFilepath) and objFilepath.endswith(".obj")):
        print("Datoteka " + objFilename + " ne postoji.")
        objFilename = input("Ime .obj datoteke objekta za ucitati (prazno za default: kocka.obj): ")
        if len(objFilename) == 0:
            return "kocka.obj"
        objFilepath = "./" + objFilename

    return objFilepath

def enterOGFilename():
    OGFilename = input("Ime .txt datoteke ocista i gledista za ucitati (prazno za default: initOG.txt): ")
    if len(OGFilename) == 0:
        return "initOG.txt"
    OGFilepath = "./" + OGFilename
    while not(path.exists(OGFilepath) and OGFilepath.endswith(".txt")):
        print("Datoteka " + OGFilename + " ne postoji.")
        OGFilename = input("Ime .txt datoteke ocista i gledista za ucitati (prazno za default: initOG.txt): ")
        if len(OGFilename) == 0:
            return "initOG.txt"
        OGFilepath = "./" + OGFilename

    return OGFilepath

def enterIsBackCull():
    ynBackCullStr = input("Zelite li uklanjanje straznjih poligona (prazno za default: ne) [da/ne]: ")
    if len(ynBackCullStr) == 0:
        return
    while ynBackCullStr.strip().lower() not in ("da", "ne"):
        ynBackCullStr = input("Zelite li uklanjanje straznjih poligona (prazno za default: ne) [da/ne]: ")
        if len(ynBackCullStr) == 0:
            return
    if ynBackCullStr.strip().lower() == "da":
        global doBackCull
        doBackCull = True

        glFrontFace(GL_CCW)
        glEnable(GL_CULL_FACE)
        glCullFace(GL_BACK)



if __name__ == "__main__":
    OGFilepath = enterOGFilename()
    print("Ucitavanje datoteke ",OGFilepath,"...")
    with open(OGFilepath, "r") as ogDescr:
        loadOG(ogDescr)


    objFilepath = enterObjFilename()
    print("Ucitavanje datoteke ", objFilepath, "...")
    with open(objFilepath, "r") as objVerticesDescr:
        loadVertices(objVerticesDescr)

    enterIsBackCull()

    xc, yc, zc, maxRange = getCenterRange(vertexDefsINIT)

    print("Translacija sredista objekta u ishodiste...")
    vertexDefsINIT = translateObject(vertexDefsINIT, -xc, -yc, -zc)

    print("Skaliranje na [-1, 1]...")
    vertexDefsINIT = scaleObject(vertexDefsINIT, 2 / maxRange)

    print()
    print("Translatiran i skaliran objekt:")
    for v in vertexDefsINIT:
        print("v {} {} {}".format(*v))
    for p in polyDefs:
        print("f {} {} {}".format(*p))


    verticesHom = [[v[0],v[1],v[2],1] for v in vertexDefsINIT]
    verticesArr = np.array(verticesHom)
    vertexDefsINIT = verticesArr

    planeCoeffsINIT = calcPlaneEquations(vertexDefsINIT)[:]
    onOChange()

    window.set_visible(True)

    pyglet.app.run()